﻿//Приложение "Автобусное расписание"
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <string>
#include <conio.h>
#include <Windows.h>

using namespace std;

const int N = 24;  //Максимальное число маршрутов в расписании
const int MaxRaces = 11; //Количество маршрутов
char Town[20];

struct Auto
{
    char City[20]; //Пункт назначения
    int Count; //Количество рейсов
    char Time[255][N]; //Время в расписании
};
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "Ru");
    Auto Autobus[MaxRaces] =
    {
        "Брянск",3,{"10:00","14:25","16:50"}, //1 число - номер маршрута, 2 число - количество рейсов, в скобках время отправления каждого рейса
        "Воронеж",1,{"08:05"},
        "Киров",5,{"10:00","12:05","14:10","17:55","20:00"},
        "Козельск",15,{"07:45","09:40","10:20","11:10","11:55","12:20","14:20","15:10","15:45","17:25","18:30","18:50","19:20","20:00","21:50",},
        "Курск",1,{"08:50"},
        "Москва",24,{"05:00","06:00","07:00","08:00","08:20","08:40","09:00","09:20","10:00","10:25","11:40","12:05","12:40","13:00","14:05","14:15","14:40","15:00","16:00","17:20","18:00","18:35","19:30","20:00"},
        "Обнинск",3,{"06:35","11:20","15:45"},
        "Орёл",2,{"07:10","16:10"},
        "Серпухов",1,{"13:50"},
        "Таруса",5,{"07:15","09:30","11:00","14:50","15:30"},
        "Тула",8,{"06:40","07:45","09:45","12:30","13:40","14:40","17:35","18:15"},
    };
    bool flag = true; //Это для обозначения, если ничего не найдется
    cout << "1. Поиск рейсов по пункту назначения\n2. Поиск рейсов по времени отправления\n";
    cout << "Введите номер операции: ";
    char x;
    cin >> x;
    switch (x)
    {
    case '1':
        cin.ignore();
        cout << "Введите пункт назначения: ";
        cin >> Town;
        for (int i = 0; i < MaxRaces; i++)
        {
            if (strcmp(Autobus[i].City, Town) == 0)
            {
                cout << "Калуга - " << Autobus[i].City << " | Отправление: ";
                for (int j = 0; j < Autobus[i].Count; j++)
                {
                    cout << Autobus[i].Time[j] << "  ";
                    flag = false;
                }
            }

        }
        cout << endl;
        break;
    case '2':
        cin.ignore();
        cout << "Введите время отправления в формате XX:XX" << '\t';
        char T[6];
        cin.getline(T, 6);
        char HH[3], MM[3];
        strncpy(MM, &T[3], 3);
        strncpy(HH, &T[0], 2);
        HH[2] = '\0';
        //Начинаем вычислять
        int Minuts = atoi(HH) * 60 + atoi(MM);
        for (int i = 0; i < MaxRaces; i++) {
            for (int j = 0; j < Autobus[i].Count; j++)
            {
                strncpy(MM, &(Autobus[i].Time[j])[3], 3);
                strncpy(HH, &(Autobus[i].Time[j])[0], 2);
                HH[2] = '\0';
                if ((atoi(HH) * 60 + atoi(MM) >= Minuts - 30) && (atoi(HH) * 60 + atoi(MM) <= Minuts + 30))
                {
                    cout << "Калуга - " << Autobus[i].City << " | Отправление: " << Autobus[i].Time[j] << "\n";
                    flag = false;
                }
            }
        }
        break;
    }
    if (flag) cout << "Ничего не найдено\n";
    system("pause");
}